package duman.eren;


import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class RedirectUnknownUrls implements ErrorController {

    @GetMapping("/error")
    public void redirectNonExistentUrlsToHome(HttpServletResponse response) throws IOException {
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "REQUESTED PATH NOT FOUND REDIRECTING ..." );
        response.sendRedirect("/");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
