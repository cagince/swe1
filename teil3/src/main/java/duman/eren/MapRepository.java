package duman.eren;

import org.springframework.data.repository.CrudRepository;

public interface MapRepository extends CrudRepository<GameMap , Long> {
}
