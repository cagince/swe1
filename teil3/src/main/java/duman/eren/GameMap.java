package duman.eren;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

@Data
    @Entity
    public class GameMap{

        private @Id
        @GeneratedValue
        Long id;

        private char[][] halfmapA;
        private char[][] halfmapB;

        private int rows;
        private int hm1CX;
        private int hm1CY;
        private int hm2CX;
        private int hm2CY;
        private int hm1TX;
        private int hm1TY;
        private int hm2TX;
        private int hm2TY;


        private GameMap() {}

        public GameMap(int rows) {
            this.rows = rows;
            this.halfmapA = populateHafmap();
            this.halfmapB = populateHafmap();
            placeCastle(true, this.halfmapA);
            placeCastle(false, this.halfmapB);
            placeTreasure(true, this.halfmapA);
            placeTreasure(false, this.halfmapB);
        }


    private char[][] transformInto2Darray(char[] halfMap) {
        char[][] map = new char[4][8];
        int lIndex = 0;
        for (int col = 0 ; col < 8; col++) {
            for (int row = 0; row < 4; row++) { map[row][col] = halfMap[lIndex++]; }
        }
        return  map;
    }
    private char[][] populateHafmap() {
        Random random = new Random();
        char[] arr = new char[8*4];
        int index = 0;
        // secures min and max amounts of mountains and waters
        int amoutOfMountains = random.nextInt((5 - 3) +1 ) + 3;
        int amoutOfWaters = random.nextInt((5 - 3) +1 ) + 3;
        // adds the water and mountain and fills the rest with grass
        for (int i = 0; i < amoutOfMountains; i++) { arr[index++]='m'; }
        for (int i = 0; i < amoutOfWaters; i++) { arr[index++]= 'w'; }
        while (index < arr.length) {
            arr[index++] = 'g';
        }
        // shuffle ArrayList
        arr = shuffleArray(shuffleArray(arr));
        return transformInto2Darray(arr);
    }

    private char[] shuffleArray(char[] array) {
        char  temp;
        int index;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
        return array;
    }

    // places castle in first row
    private void placeCastle(boolean isHalfMap1, char[][] map) {
        Random random = new Random();
        int row = isHalfMap1 ? 0 : 3;
        int col = random.nextInt(8);
        if (checkValidity(map,'g',row,col) || checkValidity(map,'b',row,col)) {
            initHalfmapCastle(isHalfMap1,row,col);
        } else {
            placeCastle(isHalfMap1, map);
        }

    }
    private void placeTreasure(boolean isHalfMap1, char[][] map) {
        Random random = new Random();
        int row = random.nextInt( 4);
        int col = random.nextInt(8);
        // not on top of castle and in grass
        if (checkValidity(map,'g',row,col)) {
            if (isHalfMap1){
                if (hm1CX != row || hm1CY != row) initHalfMapTreasure(isHalfMap1, row, col); // check if on top of castle
                else placeTreasure(isHalfMap1, map);
            } else {
                if (hm2CX != row || hm2CY != row) initHalfMapTreasure(isHalfMap1, row, col); // check if on top of castle
                else placeTreasure(isHalfMap1, map);
            }
        } else {
            placeTreasure(isHalfMap1, map);
        }
    }
    // sets castle coordinates for given halfmap
    private void initHalfmapCastle(boolean isHalfMap1, int x , int y) {
        if (isHalfMap1){

            this.hm1CX = x;
            this.hm1CY = y;
            this.halfmapA[x][y] = 'c';
        } else {
            this.hm2CX = x;
            this.hm2CY = y;
            this.halfmapB[x][y] = 'c';
        }
    }
    // sets treasure coordinates for given halfmap
    private void initHalfMapTreasure(boolean isHalfMap1, int x , int y) {

        if (isHalfMap1){
            this.hm1TX = x;
            this.hm1TY = y;
            this.halfmapA[x][y] = 't';
        } else {
            this.hm2TX = x;
            this.hm2TY = y;
            this.halfmapB[x][y] = 't';
        }

    }
    // returns true if loc = given type
    private boolean checkValidity(char[][] map, char type, int row, int col) {
        return map[row][col]  == type;
    }









}