package duman.eren;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final MapRepository repository;

    @Autowired
    public DatabaseLoader(MapRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {

        for (int i = 0 ; i <= 50; i++) {
            this.repository.save(new GameMap(8));
        }
    }
}
