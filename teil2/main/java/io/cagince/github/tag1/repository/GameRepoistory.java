package io.cagince.github.tag1.repository;

import io.cagince.github.tag1.model.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepoistory extends CrudRepository<Game, Integer>{
}
