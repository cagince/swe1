package io.cagince.github.tag1.repository;

import io.cagince.github.tag1.model.Map;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MapRepository extends CrudRepository<Map, Integer> {
}
