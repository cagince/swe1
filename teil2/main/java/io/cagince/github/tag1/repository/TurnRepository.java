package io.cagince.github.tag1.repository;

import io.cagince.github.tag1.model.Turn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurnRepository extends CrudRepository<Turn, Integer>{
}
