package io.cagince.github.tag1.service;

import io.cagince.github.tag1.model.Turn;
import io.cagince.github.tag1.repository.TurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TurnService {

    @Autowired
    TurnRepository turnRepository;

    public void addTurn(Turn turn) {
        turnRepository.save(turn);
    }
}
