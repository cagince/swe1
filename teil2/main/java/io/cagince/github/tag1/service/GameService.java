package io.cagince.github.tag1.service;


import io.cagince.github.tag1.model.*;
import io.cagince.github.tag1.repository.GameRepoistory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameService {


    @Autowired
    GameRepoistory gameRepoistory;

    @Autowired
    TurnService turnService;

    @Autowired
    PlayerService playerService;

    @Autowired
    MapService mapService;

    private Log logger = LogFactory.getLog(Exception.class);

    public Object getAllGames() {
        List<Game> games = new ArrayList<>();
        gameRepoistory.findAll().forEach(games::add);
        return games;
    }

    public Game createGame(Player player) {
        if (player.getId() == null  || playerService.getOne(player.getId()) == null){
           player = playerService.addPlayer(player);
        }
        // init Game.
        Game game = new Game(player, "blue");
        game.setMap(new Map());
        game = gameRepoistory.save(game);
        return game;
    }

    public Game registerMap(Integer game_id, Integer player_id, MapHelper mapHelper) throws Exception {

        Game game = gameRepoistory.findOne(game_id);
        if (game == null){
            throw new Exception("Game you are trying to register does not exist");
        }
        logger.info("Game has been found, adding halfmap to " + game.getId());
        boolean isHalfMapA;

        String whose = null;
        whose = game.getPlayer1().getId() == player_id ? "p1" :(game.getPlayer2().getId() == player_id? "p2" : null);
        if (whose == null ) throw new Exception("You cannot register a map, You need to join to be able to register");

        Map gameMap = game.getMap();
        logger.info("Player is valid");

        gameMap.initMap((whose.equals("p1")),
                mapHelper.getWaterCoordinates(),
                mapHelper.getMountainCoordinates(),
                mapHelper.getGrassCoordinates(),
                mapHelper.getCastleCoordinates(),
                mapHelper.getTreasureCoordinates());

        logger.info("Game map has been initialized");
        if (gameMap.getHalfmapA() != null && gameMap.getHalfmapB() != null){
            Turn initial = game.start();
            turnService.addTurn(initial);
            game.getTurns().add(initial);
        }
        mapService.save(gameMap);
        return gameRepoistory.save(game);
    }

    public Game joinGame(Integer game_id, Player player) throws Exception {

        Game game = gameRepoistory.findOne(game_id);
        if (game == null){
            throw new Exception("Game you are trying to join does not exist");
        }
        logger.info("Game has been found, joining game " + game.getId());
        if (player.getId() == null || playerService.getOne(player.getId()) == null){
            player = playerService.addPlayer(player);
            logger.info("new player has been registered id : " + player.getId());
        }

        if (!(game.getPlayer2() == null) && !(game.getPlayer1() == null)){
            throw new Exception("Game you are trying to join is already full, Sorry");
        }else if(game.getPlayer2() == null) {
           game.setPlayer2(player);
           game.setPlayer2Color("red");
           game.isIsfull();
           game = gameRepoistory.save(game);
        }else {
            throw new Exception("There is a problemi with his particular game, Please try another one");
        }

        return game;

    }

    public Game registerMove(Integer game_id, Integer player_id, TurnHelper turn) throws Exception {
        Game game = gameRepoistory.findOne(game_id);
        if (game == null){
            throw new Exception("Game you are trying register your move does not exist");
        }
        if (!game.isStarted()) throw new Exception("game has not started yet");
        if (game.isFinished()) throw new Exception("game Game is already finished");

        logger.info("Registering move to game : " + game_id);


        String whose = game.getPlayer1().getId() == player_id ? "p1" :(game.getPlayer2().getId() == player_id? "p2" : null);

        if (whose == null) throw new Exception("cannot register a turn without being in the players list");

        Turn newTurn = game.addTurn(whose.equals("p1"),turn);
        turnService.addTurn(newTurn);
        gameRepoistory.save(game);
        return game;
    }

}
