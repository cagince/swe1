package io.cagince.github.tag1.service;

import io.cagince.github.tag1.model.Map;
import io.cagince.github.tag1.repository.MapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MapService {

    @Autowired
    MapRepository mapRepository;

    public void save(Map gameMap) {
        mapRepository.save(gameMap);
    }
}
