package io.cagince.github.tag1.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="game_turn")
public class Turn {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="game_id")
    private Game game;

    private Integer turnNr;


    private String newPositionA;
    private String newPositionB;

    private String oldPositionA;
    private String oldPositionB;

    private boolean inventoryA;
    private boolean inventoryB;


    public Turn(){}




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Integer getTurnNr() {
        return turnNr;
    }

    public void setTurnNr(Integer turnNr) {
        this.turnNr = turnNr;
    }

    public String getNewPositionA() {
        return newPositionA;
    }

    public void setNewPositionA(String newPositionA) {
        this.newPositionA = newPositionA;
    }

    public String getNewPositionB() {
        return newPositionB;
    }

    public void setNewPositionB(String newPositionB) {
        this.newPositionB = newPositionB;
    }

    public String getOldPositionA() {
        return oldPositionA;
    }

    public void setOldPositionA(String oldPositionA) {
        this.oldPositionA = oldPositionA;
    }

    public String getOldPositionB() {
        return oldPositionB;
    }

    public void setOldPositionB(String oldPositionB) {
        this.oldPositionB = oldPositionB;
    }

    public boolean getInventoryA() {
        return inventoryA;
    }

    public void setInventoryA(boolean inventoryA) {
        this.inventoryA = inventoryA;
    }

    public boolean getInventoryB() {
        return inventoryB;
    }

    public void setInventoryB(boolean inventoryB) {
        this.inventoryB = inventoryB;
    }
}
