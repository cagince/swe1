package io.cagince.github.tag1.controller;

import io.cagince.github.tag1.model.Game;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.cagince.github.tag1.model.MapHelper;
import io.cagince.github.tag1.model.Player;
import io.cagince.github.tag1.model.TurnHelper;
import io.cagince.github.tag1.service.GameService;
import io.cagince.github.tag1.service.PlayerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    PlayerService playerService;

    @Autowired
    GameService gameService;

    private Log logger = LogFactory.getLog(Exception.class);



    /**
     * GET: /games
     * @return all games as json
     */
    @GetMapping(value="", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllGames(){
        logger.info("getAllGames request recieved");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE));
        return new ResponseEntity<>(gameService.getAllGames(),headers, HttpStatus.OK);
    }

    @PostMapping(value="", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity createGame(@Valid @RequestBody Player player) throws Exception{
        logger.info("createGame request by " + player.getNickname());
        Game game = gameService.createGame(player);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE));
        ResponseEntity<Game> response = new ResponseEntity<Game>(game,headers, HttpStatus.ACCEPTED);
        return response;
    }

    @PostMapping(value="/{game_id}/player/{player_id}/halfmap")
    public ResponseEntity registerHalfMap(@RequestBody MapHelper mapHelper,
                                          @PathVariable final Integer game_id,
                                          @PathVariable final Integer player_id) throws Exception{
       logger.info("register Half Map Request by player : " + player_id + " to game : " + game_id );

       Game game = gameService.registerMap(game_id, player_id, mapHelper );
       HttpHeaders headers = new HttpHeaders();
       headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE));
       ResponseEntity<Game> response = new ResponseEntity<Game>(game,headers, HttpStatus.ACCEPTED);
       return response;
    }

    @PostMapping(value = "/{game_id}/player/{player_id}/move")
    public ResponseEntity playerMoveRequest(@RequestBody TurnHelper turn,
                                            @PathVariable final Integer game_id,
                                            @PathVariable final Integer player_id) throws Exception{

        logger.info("Player move request by : " + player_id);

        Game game = gameService.registerMove(game_id, player_id, turn);

        if (game == null){
            logger.info("Game cannot be found" + game_id);
            throw new Exception("game Cannot be found, please give a valid game id");
        }else{
            logger.info("Game cannot be found" + game_id);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE));
            ResponseEntity<Game> response = new ResponseEntity<Game>(game,headers, HttpStatus.ACCEPTED);
            return response;
        }

    }


    @PostMapping(value="/{game_id}/join", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity joinGame(@Valid @RequestBody Player player, @PathVariable final Integer game_id) throws Exception {
        logger.info("Join game request by " + player.getNickname());
        Game game = gameService.joinGame(game_id, player);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE));
        ResponseEntity<Game> response = new ResponseEntity<Game>(game, headers, HttpStatus.ACCEPTED);
        return response;
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorMessage> handleError(HttpServletRequest req, Exception e){

        logger.error("Request : " + req.getRequestURL() + " raised " + e);
        ErrorMessage exceptionResponse = new ErrorMessage(e.getMessage(), "some description here");
        return new ResponseEntity<ErrorMessage>(exceptionResponse, new HttpHeaders(), HttpStatus.I_AM_A_TEAPOT);

    }

}
