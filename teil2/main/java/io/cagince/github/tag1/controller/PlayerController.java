package io.cagince.github.tag1.controller;

import io.cagince.github.tag1.model.Game;
import io.cagince.github.tag1.model.MapHelper;
import io.cagince.github.tag1.model.Player;
import io.cagince.github.tag1.model.TurnHelper;
import io.cagince.github.tag1.service.GameService;
import io.cagince.github.tag1.service.PlayerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;

@RestController
@RequestMapping("/")
public class PlayerController {


    @Autowired
    PlayerService playerService;

    @Autowired
    GameService gameService;

    private Log logger = LogFactory.getLog(Exception.class);




    /**
     * POST: /player
     * adds new user specified in req. body
     * @param player
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/player", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity addNewPlayer(@Valid @RequestBody Player player ) throws Exception {
        logger.info("addNewPlayer request recieved");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_UTF8_VALUE));
        ResponseEntity<Player> response = new ResponseEntity<Player>(playerService.addPlayer(player),headers, HttpStatus.ACCEPTED);
        return response;
    }



    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorMessage> handleError(HttpServletRequest req, Exception e){

        logger.error("Request : " + req.getRequestURL() + " raised " + e);
        ErrorMessage exceptionResponse = new ErrorMessage(e.getMessage(), "some description here");
        return new ResponseEntity<ErrorMessage>(exceptionResponse, new HttpHeaders(), HttpStatus.I_AM_A_TEAPOT);

    }


}
