package io.cagince.github.tag1.service;

import io.cagince.github.tag1.model.Player;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerServiceTest {



    @MockBean
    private PlayerService service;


    @Test
    public void addPlayer() throws Exception {
        // RemoteService has been injected into the reverser bean
        given(this.service.addPlayer(new Player("user" , "matr"))).willReturn(new Player("user", "matrnr"));
    }

    @Test
    public void getAllPlayer() throws Exception{
        given(this.service.getAllPlayers()).willReturn(new LinkedList<>());
    }

    @Test
    public void getOne() throws Exception {
        given(this.service.getOne(77)).willReturn(null);
    }



}