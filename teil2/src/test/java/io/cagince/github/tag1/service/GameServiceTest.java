package io.cagince.github.tag1.service;

import io.cagince.github.tag1.model.Game;
import io.cagince.github.tag1.model.Player;
import io.cagince.github.tag1.repository.GameRepoistory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {


    @MockBean
    private GameService service;

    @MockBean
    private GameRepoistory repoistory;

    @Test
    public void getAllGames() throws Exception {
        given(this.service.getAllGames()).willReturn(null);
    }

    @Test
    public void createGame() throws Exception {
        given(this.service.createGame(new Player("asdl","123"))).willReturn(new Game(new Player("asdl","123")));
    }
}