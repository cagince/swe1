import com.fasterxml.jackson.databind.ObjectMapper;
import io.cagince.github.tag1.controller.PlayerController;
import io.cagince.github.tag1.model.MapHelper;
import io.cagince.github.tag1.model.Player;
import io.cagince.github.tag1.model.TurnHelper;
import io.cagince.github.tag1.service.GameService;
import io.cagince.github.tag1.service.MapService;
import io.cagince.github.tag1.service.PlayerService;
import io.cagince.github.tag1.service.TurnService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ExceptionHandler;

@RunWith(SpringJUnit4ClassRunner.class)
public class PlayerControllerTest {


    private MockMvc mockMvc;


    @InjectMocks
    private io.cagince.github.tag1.controller.PlayerController controller;

    @Mock
    private PlayerService playerService;
    @Mock
    private GameService gameService;
    @Mock
    private TurnService turnService;
    @Mock
    private MapService mapService;


    @Before
    public void setup() throws Exception{

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }


    /**
     * for successful player registration, req.body should have  "nickname" and "matrNr"
     * returns 202 with user as json obj.
     */
    @Test
    public void registerNewPlayerSuccess() throws Exception{

        Player player = new Player("nickname", "12345");
        mockMvc.perform(
                MockMvcRequestBuilders.post("/player")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(player))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    /**
     * if  registernewplayer post: body doesn't have valid user parameters
     * "nickname"
     * "matrNr"
     *
     *  response should return 418. with json error message
     *
     */
    @Test
    public void registerNewPlayerFail() throws Exception{

        mockMvc.perform(
                MockMvcRequestBuilders.post("/player")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
        ).andExpect(MockMvcResultMatchers.status().isIAmATeapot())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }







    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}

