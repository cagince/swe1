import com.fasterxml.jackson.databind.ObjectMapper;
import io.cagince.github.tag1.controller.GameController;
import io.cagince.github.tag1.model.MapHelper;
import io.cagince.github.tag1.model.Player;
import io.cagince.github.tag1.model.TurnHelper;
import io.cagince.github.tag1.service.GameService;
import io.cagince.github.tag1.service.MapService;
import io.cagince.github.tag1.service.PlayerService;
import io.cagince.github.tag1.service.TurnService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@RunWith(SpringJUnit4ClassRunner.class)
public class GameControllerTest {


    private MockMvc mockMvc;


    @InjectMocks
    private GameController controller;

    @Mock
    private PlayerService playerService;
    @Mock
    private GameService gameService;
    @Mock
    private TurnService turnService;
    @Mock
    private MapService mapService;


    @Before
    public void setup() throws Exception{

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }




    /**
     *  Creation of a new game should fail if req.body doesn't have valid User informations
     * @throws Exception
     */
    @Test
    public void createNewGameFail() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/game")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
        ).andExpect(MockMvcResultMatchers.status().isIAmATeapot())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

    }



    /**
     *
     *  Creation of a new game should succeed if User is valid.
     *  response status: 202 and response body as json
     *
     * @throws Exception
     */
    @Test
    public void startNewGameSuccess() throws Exception {

        Player player = new Player("nickname", "12345");
        mockMvc.perform(
                MockMvcRequestBuilders.post("/game")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(player))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }
    /**
     *
     *  Creation of a new game should succeed if User is valid.
     *  and another user should be able to join game by giving user info.
     *
     *
     * @throws Exception
     */
    @Test
    public void joinGameSuccess() throws Exception {

        Player player1 = new Player("nickname1", "12345");
        Player player2 = new Player("nickname2", "12345");
        mockMvc.perform(
                MockMvcRequestBuilders.post("/game")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(player1))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/game/1/join")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(player2))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

    }
    /**
     *
     * Player should be able to submit map to a created game
     *
     * @throws Exception
     */
    @Test
    public void submitMap() throws Exception {

        Player player1 = new Player("nickname1", "12345");
        MapHelper mapHelper = new MapHelper();
        mapHelper.setCastleCoordinates("d3");
        mapHelper.setTreasureCoordinates("b1");
        String[] waters = { "a2", "a7", "c5", "c4"};
        mapHelper.setWaterCoordinates(waters);
        String[] mountains = { "d7", "b2", "b3" };
        String[] grass = { "a1", "d3", "d2", "b1", "c1", "d1", "c2", "a3", "c3", "a4", "b4", "d4", "a5", "b5", "d5", "a6", "b6", "c6", "d6", "b7", "c7", "a8", "b8", "c8", "d8" };
        mapHelper.setGrassCoordinates(grass);
        mapHelper.setMountainCoordinates(mountains);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/game")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(player1))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/game/1/player/1/halfmap")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(mapHelper))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }


    /**
     *
     * Player should be able to submit a move
     *
     * @throws Exception
     */
    @Test
    public void submitMove() throws Exception {

        TurnHelper turnHelper = new TurnHelper();
        turnHelper.setCurrentPosition("h3");
        turnHelper.setInventory(false);
        turnHelper.setNewPosition("h4");
        turnHelper.setTurnNumber(1);

        mockMvc.perform(
                MockMvcRequestBuilders.post("/game/1/player/1/move")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(turnHelper))
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }







    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
